/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.midterm;

/**
 *
 * @author User
 */
public class Doctor {
    private String name;
    private String occupation;
    
    public Doctor(String name,String occupation){ // Constructor คือ การกำหนด ค่าเริ่มต้น
        this.name=name;
        this.occupation=occupation;
    }
    public boolean Occupation(char occ){   // method เลือกประเภทอาชีพของ Doctor
        switch(occ){
            case 'S':  //เลือก S  เป็น Doctor  type Surgeon (เเพทย์ศัลยกรรม)
                System.out.println("Name: "+name+" Occupation: "+occupation+" Type: "+"Surgeon");
                break;
            case 'O':  //เลือก O  เป็น Doctor  type Obstetrician  (สูตินารีเเพทย์)
                 System.out.println("Name: "+name+" Occupation: "+occupation+" Type: "+"Obstetrician");
                 break;
            default: // นอกเหนือจาก S เเละ O จะไม่เป็น Doctor
                System.out.println("Name: "+name+" Not Doctor ");
                return false;
        }
        return true;
    }  
}
