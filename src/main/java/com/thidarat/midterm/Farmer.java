/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.midterm;

/**
 *
 * @author User
 */
public class Farmer {
    private String name;
    private String occupation;
    
    public Farmer(String name,String occupation){ // Constructor  คือ การกำหนด ค่าเริ่มต้น
        this.name=name;
        this.occupation=occupation;
    }
    public boolean Occupation(char occ){ // method เลือกประเภทอาชีพของ Farmer
        switch(occ){
            case 'G':  //เลือก G  เป็น Farmer type Gardener (ชาวสวน)
                System.out.println("Name: "+name+" Occupation: "+occupation+" Type: "+"Gardener");
                break;
            case 'P':  //เลือก P  เป็น Farmer type Planter (ชาวไร่)
                 System.out.println("Name: "+name+" Occupation: "+occupation+" Type: "+"Planter");
                 break;
            default: // นอกเหนือจาก G เเละ P จะไม่เป็น Farmer
                System.out.println("Name: "+name+" Not Farmer ");
                return false;
        }
        return true;
        
    }
    
        
}
