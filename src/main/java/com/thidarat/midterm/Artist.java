/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.midterm;

/**
 *
 * @author User
 */
public class Artist {
    private String name;
    private String occupation;
    
    public Artist(String name,String occupation){ // Constructor คือ การกำหนด ค่าเริ่มต้น
        this.name=name;
        this.occupation=occupation;
    }
    public boolean Occupation(char occ){ // method เลือกประเภทอาชีพของ Artist
        switch(occ){
            case 'P': //เลือก P  เป็น Artist  type Paninting  (จิตรกรรม)
                System.out.println("Name: "+name+" Occupation: "+occupation+" Type: "+"Painting");
                break;
            case 'S': //เลือก S  เป็น Artist  type Sculpture  (ประติมากรรม)
                 System.out.println("Name: "+name+" Occupation: "+occupation+" Type: "+"Sculpture");
                 break;
            default:  // นอกเหนือจาก P เเละ S จะไม่เป็น Artist
                System.out.println("Name: "+name+" Not Artist ");
                return false;
        }
        return true;
    }  
}
